'use strict';

const gulp  = require('gulp'),
      tasks = require('./gulp.tasks');

gulp.task('default', () => {
    tasks.clean();
    tasks.init();
    tasks.css();
    tasks.javascript();
    tasks.view();
});

gulp.task('development', () => {
    tasks.clean();
    tasks.init();
    tasks.css();
    tasks.javascript();
    tasks.view();

    gulp.watch('src/**/*.css', () => tasks.css());
    gulp.watch('src/**/*.js', () => tasks.javascript());
    gulp.watch('src/**/*.html', () => tasks.view());
});