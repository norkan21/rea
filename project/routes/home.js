'use strict';

const express = require('express'),
      router  = express.Router();

// ONLY one route for this code test
router.get('/', (req, res, next) => {
    res.render('home/index');
});

module.exports = router;
