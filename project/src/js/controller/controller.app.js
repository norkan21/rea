'use strict';

/**
 * Module app
**/
angular.module('app')
    .controller('appCtrl', ['$scope', 'data', ($scope, data) => {
        $scope.home = {
            results : data.results,
            saved   : data.saved
        }
    }]);