'use strict';

angular.module('app', ['ngRoute'])
    .config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) => {
        $locationProvider.html5Mode(true);

        $routeProvider.when('/', {
            templateUrl : 'view/app.html',
            controller  : 'appCtrl',
            resolve     : {
                data : ['appFactory', 'HomeResult', 'HomeSaved', (appFactory, HomeResult, HomeSaved) => {
                    let tmp = {};

                    appFactory.getResult().then((response) => {
                        tmp.results = _.map(response.data.results, (result) => { // Create HomeResult models
                            return new HomeResult(result.id, result.price, result.mainImage, result.agency);
                        });

                        tmp.saved = _.map(response.data.saved, (result) => { // Create HomeSaved models
                            return new HomeSaved(result.id, result.price, result.mainImage, result.agency);
                        });
                    });

                    return tmp;
                }]
            }
        });
    }]);