'use strict';

/**
 * Directive card
 * Use to display the property information
 *
 * How to use
 *  <card properties='home.results' saved-properties='home.saved' is-saved="true"></card>
 *
 *  1. Attribute 'properties' is the one that will be populated
 *  2. Attribute 'saved-properties' is the one that contained the list of saved properties
 *  3. Attribute 'is-saved' is used to determine whether the property is 'HomeResult' or 'HomeSaved'
 *
**/
angular.module('app')
    .directive('card', ['HomeResult', 'HomeSaved', (HomeResult, HomeSaved) => {
        return {
            restrict : 'E',
            scope    : {
                properties      : '<', // One-way is enough as data in the backend is mocked
                savedProperties : '=', // Two-way as we want the user's action to be shown in the view
                isSaved         : '@'  // Use for color of the card
            },
            template : `
                <div class='panel' ng-class='{"panel-success": isSaved, "panel-info": !isSaved}' ng-repeat='property in properties' ng-mouseenter="showAction(property)" ng-mouseleave="showAction(property)">
                    <div class='panel-heading text-center'>ID: {{:: property.id}}</div>
                    <div class='panel-body'>
                        <div class='panel-header' ng-style='{"background-color": "{{:: property.agency.brandingColors.primary}}" }'>
                            <img ng-src='{{:: property.agency.logo}}' />
                        </div>
                        <img class='card-img' ng-src='{{:: property.mainImage}}' />
                    </div>
                    <div class='panel-footer'>
                        <span>Price : {{:: property.price}}</span>
                        <button type="button" class="btn btn-success" ng-show='property.showAction && property.userAction === "add"' ng-click="addOrRemove(property)">Add Property</button>
                        <button type="button" class="btn btn-danger" ng-show='property.showAction && property.userAction === "remove"' ng-click="addOrRemove(property)">Remove Property</button>
                    </div>
                </div>
            `,
            link     : (scope) => {
                scope.isSaved = (scope.isSaved === 'true') ? true : false;

                /**
                 * Function showAction
                 * Show user's action on mouseenter.
                 * Hide user's action on mouseleave
                **/
                scope.showAction = (property) => {
                    property.showAction = !property.showAction;
                }

                /**
                 * Function addOrRemove
                **/
                scope.addOrRemove = (property) => {
                    if(property instanceof HomeResult) {
                        let isPropertyAlreadySaved = _.find(scope.savedProperties, (one) => one.id === property.id); // Test if property has been saved

                        if(!isPropertyAlreadySaved) {
                            scope.savedProperties.push(new HomeSaved(property.id, property.price, property.mainImage, property.agency));
                        } else {
                            alert(`Property ${property.id} has already been added`);
                        }
                    } else if(property instanceof HomeSaved) {
                        scope.savedProperties = _.remove(scope.savedProperties, (one) => one.id !== property.id); // Remove the property from the saved list
                    }
                }
            }
        }
    }]);