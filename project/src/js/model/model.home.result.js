'use strict';

/**
 * Class HomeResult
**/
angular.module('app')
    .factory('HomeResult', ['Home', (Home) => {
        return class HomeResult extends Home {
            constructor(id = null, price = null, mainImage = null, agency = {}, userAction = 'add', showAction = false) {
                super(id, price, mainImage, agency);
                this.userAction = userAction; // Since the property has not been added, show 'add' button instead
                this.showAction = showAction; // Determine if the 'userAction' can be shown
            }
        };
    }]);