'use strict';

/**
 * Class HomeSaved
**/
angular.module('app')
    .factory('HomeSaved', ['Home', (Home) => {
        return class HomeSaved extends Home {
            constructor(id = null, price = null, mainImage = null, agency = {}, userAction = 'remove', showAction = false) {
                super(id, price, mainImage, agency);
                this.userAction = userAction; // Since the property has been added, show 'remove' button instead
                this.showAction = showAction; // Determine if the 'userAction' can be shown
            }
        };
    }]);