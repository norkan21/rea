'use strict';

/**
 * Class Home
**/
angular.module('app')
    .factory('Home', [() => {
        return class Home {
            constructor(id = null, price = null, mainImage = null, agency = {}) {
                this.id        = id;
                this.price     = price;
                this.mainImage = mainImage;
                this.agency    = agency
            }
        };
    }]);