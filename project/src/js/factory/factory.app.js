'use strict';

/**
 * Factory
 * Contain all the api calls to the server
**/
angular.module('app')
    .factory('appFactory', ['$http', ($http) => {
        return {
            getResult: () => $http.get('/api/v1/results')
        }
    }])