'use strict';

describe('Unit test: App Controller', () => {

    let scope, controller;

    beforeEach(module('app'));

    beforeEach(inject((_$controller_, HomeResult, HomeSaved) => {
        controller = _$controller_;
        scope      = {};
        controller = controller('appCtrl', {
                        $scope : scope,
                        data   : {
                            results : [
                                    new HomeResult(10, '$21', 'http://image.com', {
                                        'logo': 'http://logo',
                                        'brandingColors': {
                                            'primary': '#ffe512'
                                        }
                                    })
                                ],
                            saved : [
                                    new HomeSaved(11, '$35', 'http://imagemagic.com', {
                                        'logo': 'http://logomagic',
                                        'brandingColors': {
                                            'primary': '#fcfa3b'
                                        }
                                    }),
                                    new HomeSaved(12, '$75', 'http://imagemagical.com', {
                                        'logo': 'http://logomagical',
                                        'brandingColors': {
                                            'primary': '#57B5E0'
                                        }
                                    })
                            ]
                        }
                    });
    }));

    it('scope should have these attributes', () => {
        expect(scope.home).toBeDefined();
        expect(scope.home.results).toBeDefined();
        expect(scope.home.saved).toBeDefined();
    });

    describe('test for attribute \'scope.results\'', () => {
        it('should have one value only', () => {
            expect(scope.home.results.length).toEqual(1);
        });

        it('should have the right id', () => {
            expect(scope.home.results[0].id).toEqual(10);
        });

        it('should have the right amount', () => {
            expect(scope.home.results[0].price).toEqual('$21');
        });

        it('should have the right image', () => {
            expect(scope.home.results[0].mainImage).toEqual('http://image.com');
        });

        it('should have the right logo', () => {
            expect(scope.home.results[0].agency.logo).toEqual('http://logo');
        });

        it('should have the right primary brandingColors', () => {
            expect(scope.home.results[0].agency.brandingColors.primary).toEqual('#ffe512');
        });

        it('should have the right userAction', () => {
            expect(scope.home.results[0].userAction).toEqual('add');
        });

        it('should have the right showAction', () => {
            expect(scope.home.results[0].showAction).toEqual(false);
        });
    });

    describe('test for attribute \'scope.saved\'', () => {
        it('should have two values', () => {
            expect(scope.home.saved.length).toEqual(2);
        });

        it('should have the right id', () => {
            expect(scope.home.saved[0].id).toEqual(11);
            expect(scope.home.saved[1].id).toEqual(12);
        });

        it('should have the right amount', () => {
            expect(scope.home.saved[0].price).toEqual('$35');
            expect(scope.home.saved[1].price).toEqual('$75');
        });

        it('should have the right image', () => {
            expect(scope.home.saved[0].mainImage).toEqual('http://imagemagic.com');
            expect(scope.home.saved[1].mainImage).toEqual('http://imagemagical.com');
        });

        it('should have the right logo', () => {
            expect(scope.home.saved[0].agency.logo).toEqual('http://logomagic');
            expect(scope.home.saved[1].agency.logo).toEqual('http://logomagical');
        });

        it('should have the right primary brandingColors', () => {
            expect(scope.home.saved[0].agency.brandingColors.primary).toEqual('#fcfa3b');
            expect(scope.home.saved[1].agency.brandingColors.primary).toEqual('#57B5E0');
        });

        it('should have the right userAction', () => {
            expect(scope.home.saved[0].userAction).toEqual('remove');
            expect(scope.home.saved[1].userAction).toEqual('remove');
        });

        it('should have the right showAction', () => {
            expect(scope.home.saved[0].showAction).toEqual(false);
            expect(scope.home.saved[1].showAction).toEqual(false);
        });
    });

});