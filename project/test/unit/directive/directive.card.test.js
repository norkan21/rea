'use strict';

describe('Unit test: Directive card', () => {

    let compile, scope;

    beforeEach(module('app'));

    beforeEach(inject((_$compile_, _$rootScope_, HomeResult, HomeSaved) => {
        compile = _$compile_;
        scope = _$rootScope_;

        scope.results = [];
        scope.saved   = [];

        scope.results.push(
                new HomeResult(10, '$21', 'http://image.com', {
                    'logo': 'http://logo',
                    'brandingColors': {
                        'primary': '#ffe512'
                    }
                })
            );

        scope.saved.push(
                new HomeSaved(11, '$11', 'http://imagemagical.com', {
                    'logo': 'http://logo.com',
                    'brandingColors': {
                        'primary': '#ffe612'
                    }
                })
            );
    }));

    describe('should create the \'result\'', () => {
        let element;

        beforeEach(() => {
            element = compile('<card properties="results" saved-properties="saved"></card>')(scope);
            scope.$digest();
        });

        it('should have ID', () => {
            expect(element.find('div.panel-heading').text()).toBe('ID: 10');
        });

        it('should have a background-color', () => {
            expect(element.find('div.panel-header').attr('style')).toBe('background-color: rgb(255, 229, 18);');
        });

        it('should have a logo', () => {
            expect(element.find('div.panel-header').find('img').attr('src')).toBe('http://logo');
        });

        it('should have an image', () => {
            expect(element.find('img.card-img').attr('src')).toEqual('http://image.com');
        });

        it('should have a Price', () => {
            expect(element.find('div.panel-footer').find('span').text()).toBe('Price : $21');
        });

        it('should have an add button', () => {
            expect(element.find('button.btn-success').attr('class')).toBe('btn btn-success ng-hide');
            expect(element.find('button.btn-danger').attr('class')).toBe('btn btn-danger ng-hide');
        });
    });

    describe('should create the \'saved\'', () => {
        let element;

        beforeEach(() => {
            element = compile('<card properties="saved" saved-properties="saved" is-saved="true"></card>')(scope);
            scope.$digest();
        });

        it('should have ID', () => {
            expect(element.find('div.panel-heading').text()).toBe('ID: 11');
        });

        it('should have a background-color', () => {
            expect(element.find('div.panel-header').attr('style')).toBe('background-color: rgb(255, 230, 18);');
        });

        it('should have a logo', () => {
            expect(element.find('div.panel-header').find('img').attr('src')).toBe('http://logo.com');
        });

        it('should have an image', () => {
            expect(element.find('img.card-img').attr('src')).toBe('http://imagemagical.com');
        });

        it('should have a Price', () => {
            expect(element.find('div.panel-footer').find('span').text()).toBe('Price : $11');
        });

        it('should have an add button', () => {
            expect(element.find('button.btn-success').attr('class')).toBe('btn btn-success ng-hide');
            expect(element.find('button.btn-danger').attr('class')).toBe('btn btn-danger ng-hide');
        });
    });

    describe('should add property', () => {
        let element;

        beforeEach(() => {
            element = compile('<card properties="results" saved-properties="saved" is-saved="true"></card>')(scope);
            scope.$digest();
        });

        it('add property', () => {
            expect(scope.saved.length).toBe(1);
            element.find('button.btn-success').triggerHandler('click');
            expect(scope.saved.length).toBe(2);
        });

        it('add same property', () => {
            expect(scope.saved.length).toBe(1);
            element.find('button.btn-success').triggerHandler('click');
            expect(scope.saved.length).toBe(2);
            element.find('button.btn-success').triggerHandler('click');
            expect(scope.saved.length).toBe(2);
        });
    });

    describe('should remove property in \'saved\'', () => {
        let element;

        beforeEach(() => {
            element = compile('<card properties="saved" saved-properties="saved" is-saved="true"></card>')(scope);
            scope.$digest();
        });

        it('remove property', () => {
            expect(scope.saved.length).toBe(1);
            element.find('button.btn-danger').triggerHandler('click');
            expect(scope.saved.length).toBe(0);
        });
    });
});