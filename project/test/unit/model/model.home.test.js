'use strict';

describe('Unit test: Model Home', () => {

    let home;

    beforeEach(module('app'));

    beforeEach(inject((Home) => {
        home = new Home(120, '$40', 'http://image.com', {
                    'brandingColors': {
                        'primary': '#000000'
                    },
                    'logo': 'http://logo.com'
                });
    }));

    it('scope should have these attributes', () => {
        expect(home).toBeDefined();
        expect(home.id).toBeDefined();
        expect(home.price).toBeDefined();
        expect(home.mainImage).toBeDefined();
        expect(home.agency).toBeDefined();
    });

    it('should have the right id', () => {
        expect(home.id).toEqual(120);
    });

    it('should have the right amount', () => {
        expect(home.price).toEqual('$40');
    });

    it('should have the right image', () => {
        expect(home.mainImage).toEqual('http://image.com');
    });

    it('should have the right logo', () => {
        expect(home.agency.logo).toEqual('http://logo.com');
    });

    it('should have the right primary brandingColors', () => {
        expect(home.agency.brandingColors.primary).toEqual('#000000');
    });
});