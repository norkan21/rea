'use strict';

describe('Unit test: Model HomeSaved', () => {

    let home;

    beforeEach(module('app'));

    beforeEach(inject((HomeSaved) => {
        home = new HomeSaved(120, '$40', 'http://image.com', {
                    'brandingColors': {
                        'primary': '#000000'
                    },
                    'logo': 'http://logo.com'
                });
    }));

    it('scope should have these attributes', () => {
        expect(home).toBeDefined();
        expect(home.id).toBeDefined();
        expect(home.price).toBeDefined();
        expect(home.mainImage).toBeDefined();
        expect(home.agency).toBeDefined();
        expect(home.userAction).toBeDefined();
        expect(home.showAction).toBeDefined();
    });

    it('should have the right id', () => {
        expect(home.id).toEqual(120);
    });

    it('should have the right amount', () => {
        expect(home.price).toEqual('$40');
    });

    it('should have the right image', () => {
        expect(home.mainImage).toEqual('http://image.com');
    });

    it('should have the right logo', () => {
        expect(home.agency.logo).toEqual('http://logo.com');
    });

    it('should have the right primary brandingColors', () => {
        expect(home.agency.brandingColors.primary).toEqual('#000000');
    });

    it('should have the right userAction', () => {
        expect(home.userAction).toEqual('remove');
    });

    it('should have the right showAction', () => {
        expect(home.showAction).toEqual(false);
    });
});